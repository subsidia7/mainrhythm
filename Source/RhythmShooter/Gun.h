// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/WidgetComponent.h"
#include "HUDS/MushkaWidget.h"

#include "GameFramework/Actor.h"
#include "Components/SkeletalMeshComponent.h"
#include "TimerManager.h"
#include "Particles/ParticleSystemComponent.h"
#include "Gun.generated.h"

UCLASS()
class RHYTHMSHOOTER_API AGun : public AActor
{
	GENERATED_BODY()
public:
	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
		class USkeletalMeshComponent* FP_Gun;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
		TSubclassOf<class ARhythmShooterProjectile> ProjectileClass;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* FP_MuzzleLocation;

	/** Particles for gun oveheat*/
	UPROPERTY(VisibleDefaultsOnly, Category = Particles)
		UParticleSystemComponent* Particles_For_Overheat;
	bool isOverheat;
	
	

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		class USoundBase* FireSound;

	inline USkeletalMesh* GetMesh() { return FP_Gun->SkeletalMesh; };
	inline TSubclassOf<class ARhythmShooterProjectile> GetProjectileClass() { return ProjectileClass; };
	inline USceneComponent* GetMuzzleLocation() { return FP_MuzzleLocation; };
	inline USoundBase* GetSoundBase() { return FireSound; };
	// Sets default values for this actor's properties
	AGun();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Gun parameters
	void SetParameters();
	/** Total Shot Damage */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		float damage;

	/** The number of hits on the enemy before the damage upgrade */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		int hitsToDamageUpgrade;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		float damageUpgrade;

	/** Potential Shots Per Second */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		float RateOfFire;
	float delayTime;	
	bool canFire;
	FTimerHandle fireDelayTimerHandle;

public:

	/** The number of shells in one clip */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		int capacity;

	/** The angle within which projectiles fly after firing */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		float Spread;

	/** Shells in one shot */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		int ammoPerShot;

	/** Projectile speed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		float ProjectileSpeed;

	/** Full recharge time from overheat */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		float fullReloadTime;

	/** Force of return in arbitrary units */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		float recoil;

	/** Number of through damages */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Parameters)
		int throughDamage;

	/** "Mushka" widget class for this gun */
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UMushkaWidget> MushkaClass;

	/** "Icon" widget class for this gun */
	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
		TSubclassOf<UUserWidget> IconClass;

	/** Mushka for this gun*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UMushkaWidget* mushkaRef;
	//Create mushka widget for this gun
	void CreateMushka();

};
