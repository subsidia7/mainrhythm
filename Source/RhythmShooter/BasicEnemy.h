// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Gun.h"
#include <vector>
#include "Rhythm/RhythmCharacter.h"
#include "BasicEnemy.generated.h"

UCLASS()
class RHYTHMSHOOTER_API ABasicEnemy : public ARhythmCharacter
{
	GENERATED_BODY()

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		class USceneComponent* ENEMY_MuzzleLocation;

public:
	// Sets default values for this character's properties
	ABasicEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION(BlueprintCallable)
	void OnFire();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float maxHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float currentHealth;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bDead;

	static TSubclassOf<class APickupActor> DropItemClass;
	UFUNCTION(BlueprintCallable)
		static void SetDropItemClass(TSubclassOf<class APickupActor> ItemClass);

	virtual void DropItem();

	TSubclassOf<class ARhythmShooterProjectile> ProjectileClass;
	class USoundBase* FireSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		FVector GunOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gun)
		TSubclassOf<class AGun> EnemyGunClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Gun)
		AGun* enemyGun;

	/**Set gun from gunclass if it is not set*/
	UFUNCTION(BlueprintCallable)
		void SetGun();

	/**Set muzzlelocation gun (BE VERY CAREFUL)*/
	UFUNCTION(BlueprintCallable)
		void SetMuzzleLocation();

	UFUNCTION(BlueprintCallable)
		void SetGunRef(AGun* gunRef);

	void Death();

	UFUNCTION(BlueprintImplementableEvent)
		void ChangeMaterialState();

	
	UFUNCTION(BlueprintCallable)
		static void SetZeroEnemiesCount();
	UFUNCTION(BlueprintCallable)
		static int GetEnemiesCount();
	UFUNCTION(BlueprintCallable)
		static void IncreaseEnemiesCount();
	UFUNCTION(BlueprintCallable)
		static void DecreaseEnemiesCount();

	static int EnemiesCount;
	/** Count of Enemies in last wave*/
	static int WaveEnemiesCount;

	UFUNCTION(BlueprintCallable)
		static void SetWaveEnemiesCount(int count);
	UFUNCTION(BlueprintCallable)
		static int GetWaveEnemiesCount();

	UFUNCTION(BlueprintCallable)
		static void IncreaseWaveEnemiesCount(int count);

	UFUNCTION(BlueprintCallable)
		static void SetLevelTrigger(ALevelTrigger* trigger);
	UFUNCTION(BlueprintCallable)
		static ALevelTrigger* GetLevelTrigger();

	static class ALevelTrigger* LevelTrigger;

	void GetDamage(float damage);

};
