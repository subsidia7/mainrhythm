// Fill out your copyright notice in the Description page of Project Settings.

#include "BasicEnemy.h"
#include "Kismet/GameplayStatics.h"
#include "RhythmShooterProjectile.h"
#include "Items/PickupActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "LevelTrigger.h"

int ABasicEnemy::EnemiesCount = 0;
int ABasicEnemy::WaveEnemiesCount = 0;
TSubclassOf<APickupActor> ABasicEnemy::DropItemClass;
ALevelTrigger* ABasicEnemy::LevelTrigger = nullptr;

// Sets default values
ABasicEnemy::ABasicEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;



	GunOffset = FVector(100.0f, 0.0f, 10.0f);
}

// Called when the game starts or when spawned
void ABasicEnemy::BeginPlay()
{
	Super::BeginPlay();
	currentHealth = maxHealth;
	ABasicEnemy::IncreaseEnemiesCount();
}

void ABasicEnemy::SetGun()
{
	if (EnemyGunClass)
	{
		enemyGun = EnemyGunClass.GetDefaultObject();
		if (enemyGun)
		{
			FireSound = enemyGun->GetSoundBase();
			ProjectileClass = enemyGun->GetProjectileClass();
		}
	}
}

void ABasicEnemy::SetMuzzleLocation()
{
	if (enemyGun)
	{
		ENEMY_MuzzleLocation->SetRelativeLocation(enemyGun->GetMuzzleLocation()->RelativeLocation);
	}
}

void ABasicEnemy::SetGunRef(AGun* gunRef)
{
	enemyGun = gunRef;
	if (enemyGun)
	{
		FireSound = enemyGun->GetSoundBase();
		ProjectileClass = enemyGun->GetProjectileClass();
		//ENEMY_MuzzleLocation->SetRelativeLocation(enemyGun->GetMuzzleLocation()->RelativeLocation);
	}
}

// Called every frame
void ABasicEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABasicEnemy::GetDamage(float damage)
{
	currentHealth -= damage;
	if (currentHealth <= 0 && !bDead)
	{
		Death();
	}
}

// Called to bind functionality to input
void ABasicEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABasicEnemy::OnFire()
{

	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			FRotator SpawnRotation = GetControlRotation();
			float angle = enemyGun->Spread / 2.0;
			FVector projectileDir = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(SpawnRotation.Vector(), angle);
			SpawnRotation = projectileDir.Rotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = ((ENEMY_MuzzleLocation != nullptr) ? ENEMY_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

			// spawn the projectile at the muzzle
			ARhythmShooterProjectile* newProjectile = World->SpawnActor<ARhythmShooterProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			float ammoDamage = enemyGun->damage / enemyGun->ammoPerShot;
			if (newProjectile)
				newProjectile->SetFireParameters(ammoDamage, enemyGun->ProjectileSpeed, 0);			
		}
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}
}

void ABasicEnemy::Death()
{
	GetMesh()->SetCollisionProfileName(TEXT("Ragdoll"));
	SetActorEnableCollision(true);
	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->WakeAllRigidBodies();
	GetMesh()->bBlendPhysics = true;
	bDead = true;
	ABasicEnemy::DecreaseEnemiesCount();
	double dividing = 1; 
	if(ABasicEnemy::WaveEnemiesCount != 0)
		dividing = ABasicEnemy::EnemiesCount / (double)ABasicEnemy::WaveEnemiesCount;
	if (ABasicEnemy::EnemiesCount == 0)
	{
		if (LevelTrigger)
		{
			if (!LevelTrigger->bBabyWithItemSpawned)
			{
				LevelTrigger->SpawnBabyWithItem();
				LevelTrigger->bBabyWithItemSpawned = true;
			}
			else 
			{
				DropItem();
				ABasicEnemy::LevelTrigger->WaveEnd();
			}
		}
	}
	else if (dividing <= 0.2)
	{
		if(LevelTrigger)
			ABasicEnemy::LevelTrigger->SpawnWave();
	}
	//GetWorld()->GetTimerManager().SetTimer(DeadTimerHandle, this, &ABasicEnemy::DestroySelf, 5.0f, false);

}

void ABasicEnemy::DropItem()
{
	FRotator rotation = GetControlRotation();
	// spawn distance
	FVector rotator = rotation.RotateVector(FVector(100, 0, 0));
	
	FVector location = GetActorLocation();
	location = location + FVector(0, 0, BaseEyeHeight);
	FVector SpawnTransformLocation = location + rotator;

	GetWorld()->SpawnActor<APickupActor>(DropItemClass, SpawnTransformLocation, FRotator::ZeroRotator);
}

void ABasicEnemy::SetLevelTrigger(ALevelTrigger* trigger)
{
	ABasicEnemy::LevelTrigger = trigger;
}

void ABasicEnemy::SetZeroEnemiesCount()
{
	ABasicEnemy::EnemiesCount = 0;
}

void ABasicEnemy::SetWaveEnemiesCount(int count)
{
	ABasicEnemy::WaveEnemiesCount = count;
}

int ABasicEnemy::GetWaveEnemiesCount()
{
	return ABasicEnemy::WaveEnemiesCount;
}

ALevelTrigger* ABasicEnemy::GetLevelTrigger()
{
	return ABasicEnemy::LevelTrigger;
}

void ABasicEnemy::IncreaseWaveEnemiesCount(int count)
{
	ABasicEnemy::WaveEnemiesCount += count;
}

int ABasicEnemy::GetEnemiesCount()
{
	return ABasicEnemy::EnemiesCount;
}

void ABasicEnemy::IncreaseEnemiesCount()
{
	ABasicEnemy::EnemiesCount++;
}

void ABasicEnemy::DecreaseEnemiesCount()
{
	ABasicEnemy::EnemiesCount--;
}

void ABasicEnemy::SetDropItemClass(TSubclassOf<APickupActor> subclass)
{
	ABasicEnemy::DropItemClass = subclass;
}
