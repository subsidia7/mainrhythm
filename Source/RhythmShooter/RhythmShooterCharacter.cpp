// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "RhythmShooterCharacter.h"
#include "Items/PickupActor.h"
#include "RhythmShooterProjectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "FileHelper.h"
#include "HUDS/RhythmCrosshair.h"
#include "HUDS/RhythmPerfection.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ARhythmShooterCharacter

ARhythmShooterCharacter::ARhythmShooterCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);
	
	IsDeath = false;

	//Create a death mesh
	DeathMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("DeathMesh"));
	DeathMesh->SetVisibility(false);
	DeathMesh->SetupAttachment(GetCapsuleComponent());
	DeathMesh->bCastDynamicShadow = false;
	DeathMesh->CastShadow = false;
	DeathMesh->SetCollisionProfileName(TEXT("Ragdoll"));
	DeathMesh->RelativeLocation = FVector(109.0f, 0.0f, -90.0f);

	//Create s death springarm component
	DeathSpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("DeathSpringArm"));
	DeathSpringArmComponent->SetupAttachment(GetCapsuleComponent());
	DeathSpringArmComponent->bDoCollisionTest = false;

	////Create a death camera component
	DeathCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("DeathCamera"));
	DeathCameraComponent->SetupAttachment(DeathSpringArmComponent);
	DeathCameraComponent->SetAutoActivate(false);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	Particles_For_Overheat = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticlesLocation"));
	Particles_For_Overheat->SetupAttachment(FP_Gun);

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;

	LevelMusicComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SoundComponent"));
	SetOrdinaryFireState();	

	MaxUseDistance = 800;
	MaxEnemyOutlineDistance = 800;
	bHasNewFocus = true;
}

void ARhythmShooterCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	CurrentHealth = MaxHealth;

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
	
	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}
	// Load guns
	for (int i = 0; i < GunsArray.Num(); i++)
	{
		Guns.Push(GunsArray[i].GetDefaultObject());
		Guns[i]->SetParameters();
		if (Guns[i]->IconClass)
			GunsIcons.Push(CreateWidget<UUserWidget>(GetWorld(), Guns[i]->IconClass));
		else GunsIcons.Push(nullptr);
		/*if (Guns[i]->MushkaClass)
		{
			mushka.Push(CreateWidget<UMushkaWidget>(GetWorld(), Guns[i]->MushkaClass));
			mushka[i]->AddToViewport();
			mushka[i]->SetVisibility(ESlateVisibility::Hidden);
		}
		else mushka.Push(nullptr);*/
	}
	// Sets gun for a begin playing
	SetGun();
	numberOfMisses = 0;
	numberOfRitmMisses = 0;
	hitInfoArray.AddDefaulted(GunsArray.Num() * 3);
	for (int i = 0; i < hitInfoArray.Num(); i++)
		hitInfoArray[i].isHitEnemy = true;	

	//Set widget for blood on screen
	if (BloodWidgetClass)
	{
		BloodWidget = CreateWidget<UOneImageWidget>(GetWorld(), BloodWidgetClass);
		if (BloodWidget)
			BloodWidget->AddToViewport(-10);
	}
}

//////////////////////////////////////////////////////////////////////////
// Gun changing process

void ARhythmShooterCharacter::NextGun()
{
	//if (mushka[iCurrentGun] != nullptr)
	//	mushka[iCurrentGun]->SetVisibility(ESlateVisibility::Hidden);
	iCurrentGun = (iCurrentGun + 1) % Guns.Num();
	SetGun();
}

void ARhythmShooterCharacter::PrevGun()
{
	/*if (mushka[iCurrentGun] != nullptr)
		mushka[iCurrentGun]->SetVisibility(ESlateVisibility::Hidden);*/
	iCurrentGun = (iCurrentGun - 1 + Guns.Num()) % Guns.Num();
	SetGun();
}

void ARhythmShooterCharacter::SetGunWithIndex(int index)
{
	if (index != iCurrentGun)
	{
		iCurrentGun = index;
		SetGun();
	}
}


void ARhythmShooterCharacter::SetGun()
{
	FP_Gun->SetSkeletalMesh(Guns[iCurrentGun]->GetMesh());
	FireSound = Guns[iCurrentGun]->GetSoundBase();
	FP_MuzzleLocation->SetRelativeLocation(Guns[iCurrentGun]->GetMuzzleLocation()->RelativeLocation);
	ProjectileClass = Guns[iCurrentGun]->GetProjectileClass();

	//Particles for overheat
	Particles_For_Overheat->SetRelativeLocation(Guns[iCurrentGun]->Particles_For_Overheat->RelativeLocation);
	Particles_For_Overheat->SetTemplate(Guns[iCurrentGun]->Particles_For_Overheat->Template);	
	if (Guns[iCurrentGun]->isOverheat)
		Particles_For_Overheat->Activate();
	else
		Particles_For_Overheat->Deactivate();

	//Set "Mushka" for gun		
	/*if (mushka[iCurrentGun] != nullptr)
	{
		mushka[iCurrentGun]->SetVisibility(ESlateVisibility::Visible);
	}*/
}

//////////////////////////////////////////////////////////////////////////
// Input

void ARhythmShooterCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	InputComponent->BindAction("PickUp", IE_Pressed, this, &ARhythmShooterCharacter::PickUp);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ARhythmShooterCharacter::OnFire);

	// Bind gun change event
	PlayerInputComponent->BindAction("NextGun", IE_Pressed, this, &ARhythmShooterCharacter::NextGun);
	PlayerInputComponent->BindAction("PrevGun", IE_Pressed, this, &ARhythmShooterCharacter::PrevGun);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ARhythmShooterCharacter::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ARhythmShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARhythmShooterCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ARhythmShooterCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ARhythmShooterCharacter::LookUpAtRate);
}

void ARhythmShooterCharacter::DecreaseCombo()
{
	if (Combo > 0)
		Combo--;
}

void ARhythmShooterCharacter::IncreaseCombo()
{
	Combo++;
}

void ARhythmShooterCharacter::OnFire()
{
	(this->*FireFunction)();
}

void ARhythmShooterCharacter::SetOrdinaryFireState()
{
	FireFunction = &ARhythmShooterCharacter::OrdinaryFire;
}

void ARhythmShooterCharacter::SetRhythmFireState()
{
	FireFunction = &ARhythmShooterCharacter::RhythmFire;
}

// return true if gun canFire
void ARhythmShooterCharacter::OrdinaryFire()
{
	// try and fire a projectile
	if (Guns[iCurrentGun]->canFire)
	{
		//		HUD->ChangeCombo();
		if (Guns[iCurrentGun]->delayTime > 0)
			Guns[iCurrentGun]->canFire = false;

		if (ProjectileClass != NULL)
		{
			UWorld* const World = GetWorld();
			if (World != NULL)
			{
				if (bUsingMotionControllers)
				{
					const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
					const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
					World->SpawnActor<ARhythmShooterProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
				}
				else
				{		
					//For check hits
					CharacterProjHitInfo* projInfo = FindFreeHitElement();
					GetWorld()->GetTimerManager().SetTimer(projInfo->hitCheckTimer, this, &ARhythmShooterCharacter::CheckHits, 1.5, false);

					for (int ammo = 0; ammo < Guns[iCurrentGun]->ammoPerShot; ammo++)
					{
						FRotator SpawnRotation = GetControlRotation();
						float angle = Guns[iCurrentGun]->Spread / 2.0;
						FVector projectileDir = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(SpawnRotation.Vector(), angle);
						SpawnRotation = projectileDir.Rotation();
						// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
						const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

						//Set Spawn Collision Handling Override
						FActorSpawnParameters ActorSpawnParams;
						ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

						// spawn the projectile at the muzzle
						ARhythmShooterProjectile* newProjectile = World->SpawnActor<ARhythmShooterProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
						float comboDamageUpgrade = 0;
						if(Guns[iCurrentGun]->hitsToDamageUpgrade > 0)
							comboDamageUpgrade = (Combo / Guns[iCurrentGun]->hitsToDamageUpgrade) * Guns[iCurrentGun]->damageUpgrade;
						float ammoDamage = (Guns[iCurrentGun]->damage + comboDamageUpgrade) / Guns[iCurrentGun]->ammoPerShot;
						
						if (newProjectile)
						{
							newProjectile->SetFireParameters(ammoDamage, Guns[iCurrentGun]->ProjectileSpeed, Guns[iCurrentGun]->throughDamage);
							newProjectile->ProjectileClass = Guns[iCurrentGun]->ProjectileClass;
							newProjectile->hitInfo = projInfo;
							newProjectile->owner = this;
						}
					}
				}
			}
		}

		// try and play the sound if specified
		if (FireSound != NULL)
		{
			UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
		}

		// try and play a firing animation if specified
		if (FireAnimation != NULL && !bIsAiming)
		{
			// Get the animation object for the arms mesh
			UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
			if (AnimInstance != NULL)
			{
				AnimInstance->Montage_Play(FireAnimation, 1.f);
			}
		}

		if (Guns[iCurrentGun]->delayTime > 0)
			GetWorld()->GetTimerManager().SetTimer(Guns[iCurrentGun]->fireDelayTimerHandle, this, &ARhythmShooterCharacter::AllowFire, Guns[iCurrentGun]->delayTime, false);
		// ����������� ������� ������� ���� ���
		CanFire = true;
	}
}

void ARhythmShooterCharacter::RhythmFire()
{
	OrdinaryFire();
	if (CanFire && Crosshair->InRhythmPressed())
	{
		IncreaseCombo();
		if (ComboInLimitArray())
		{
			LimitArrayIndex++;
		}
		VolumeMutliplier += VolumeMutliplierAdding;
		LevelMusicComponent->VolumeMultiplier = VolumeMutliplier;
		numberOfRitmMisses = 0;
		SpawnRhythmPerfection();

	}
	else
	{
		//DecreaseCombo();
		if (!Guns[iCurrentGun]->isOverheat)
		{
			if(CanFire)
				SpawnRhythmPerfection();
			numberOfRitmMisses++;
			if (numberOfRitmMisses > MaxNumberOfRitmMisses)
			{
				Combo = 0;
				LimitArrayIndex = 0;
				VolumeMutliplier = 1;
				LevelMusicComponent->VolumeMultiplier = VolumeMutliplier;
				numberOfRitmMisses = 0;
				if (Guns[iCurrentGun]->fullReloadTime > 0)
				{
					GetWorld()->GetTimerManager().SetTimer(Guns[iCurrentGun]->fireDelayTimerHandle, this, &ARhythmShooterCharacter::AllowFireAfterOverheat, Guns[iCurrentGun]->fullReloadTime, false);
					Guns[iCurrentGun]->isOverheat = true;
					Guns[iCurrentGun]->canFire = false;
					if (Particles_For_Overheat->Template)
						Particles_For_Overheat->Activate();
				}
			}
		}
	}
	// ����������� ������� ������� ���� ���
	CanFire = false;
}

void ARhythmShooterCharacter::SpawnRhythmPerfection()
{
	FVector Location = GetActorLocation() + FirstPersonCameraComponent->GetForwardVector() * PerfectionDistance;
	ARhythmPerfection* rf = GetWorld()->SpawnActor<ARhythmPerfection>(RhythmPerfectionClass, Location, FRotator::ZeroRotator);
	rf->SetPlayer(this);
	FPerfection perf = Crosshair->GetPerfection();
	rf->SetTextAndColor(perf);
}


bool ARhythmShooterCharacter::ComboInLimitArray()
{
	if (LimitArrayIndex == ComboLimitArray.Num() || LimitArrayIndex == -1)
		return false;
	return Combo == ComboLimitArray[LimitArrayIndex];
}

void ARhythmShooterCharacter::SetLvlMusic(USoundCue* LvlMusic)
{
	VolumeMutliplier = 1;
	LevelMusicComponent->SetSound(LvlMusic);
	LevelMusicComponent->SetVolumeMultiplier(VolumeMutliplier);
}

void ARhythmShooterCharacter::SetLvlMusicComp(UAudioComponent * LvlMusic)
{
	VolumeMutliplier = 1;
	LevelMusicComponent->Sound = LvlMusic->Sound;
	LevelMusicComponent->SetVolumeMultiplier(VolumeMutliplier);
}

void ARhythmShooterCharacter::MyTakeDamage(float damage)
{
	if (!IsDeath)
	{
		CurrentHealth -= damage;
		if (CurrentHealth <= 0)
		{
			IsDeath = true;
			Death();
		}
	}
}

void ARhythmShooterCharacter::Death()
{
	GetCapsuleComponent()->SetCapsuleRadius(0.0f);
	GetCapsuleComponent()->SetCapsuleHalfHeight(0.0f);
	bIsAiming = false;
	Mesh1P->SetVisibility(false);
	DeathMesh->SetVisibility(true);
	DeathMesh->SetAllBodiesSimulatePhysics(true);
	DeathMesh->SetAllBodiesPhysicsBlendWeight(true);

	FirstPersonCameraComponent->SetAutoActivate(false);
	FirstPersonCameraComponent->Deactivate();
	DeathCameraComponent->Activate();
	
	//if (mushka[iCurrentGun] != nullptr)
	//	mushka[iCurrentGun]->SetVisibility(ESlateVisibility::Hidden);
	DeathWidget = CreateWidget<UUserWidget>(GetWorld(), DeathWidgetClass);
	DeathWidget->AddToViewport();
	GetWorld()->GetFirstPlayerController()->SetInputMode(FInputModeUIOnly::FInputModeUIOnly());
	GetWorld()->GetFirstPlayerController()->bShowMouseCursor = true;

	if (BloodWidget)
		BloodWidget->RemoveFromParent();
}

CharacterProjHitInfo* ARhythmShooterCharacter::FindFreeHitElement()
{
	for(int i=0;i<hitInfoArray.Num();i++)
		if (hitInfoArray[i].isHitEnemy && GetWorld()->GetTimerManager().GetTimerRemaining(hitInfoArray[i].hitCheckTimer) == 0)
		{
			hitInfoArray[i].isHitEnemy = false;
			return &hitInfoArray[i];
		}
	hitInfoArray.AddDefaulted(1);
	hitInfoArray[hitInfoArray.Num() - 1].isHitEnemy = false;
	return &hitInfoArray[hitInfoArray.Num()-1];
}

void ARhythmShooterCharacter::CheckHits()
{
	for (int i = 0; i < hitInfoArray.Num(); i++)
		if (GetWorld()->GetTimerManager().GetTimerRemaining(hitInfoArray[i].hitCheckTimer) < 0.0001)
		{
			if (!hitInfoArray[i].isHitEnemy)
			{
				if(CheckMisses)
					numberOfMisses++;
				hitInfoArray[i].isHitEnemy = true;
				if (numberOfMisses > MaxNumberOfEnemyMisses)
				{
					numberOfMisses = 0;
					Combo = 0;
				}
			}
		}
}

void ARhythmShooterCharacter::AllowFire()
{
	for(int i = 0; i<Guns.Num();i++)
		if (GetWorld()->GetTimerManager().GetTimerRemaining(Guns[i]->fireDelayTimerHandle) < 0.001)
		{
			Guns[i]->canFire = true;
			GetWorldTimerManager().ClearTimer(Guns[i]->fireDelayTimerHandle);
		}
}

void ARhythmShooterCharacter::AllowFireAfterOverheat()
{
	for (int i = 0; i < Guns.Num(); i++)
		if (Guns[i]->isOverheat && GetWorld()->GetTimerManager().GetTimerRemaining(Guns[i]->fireDelayTimerHandle) < 0.001)
		{
			Guns[i]->canFire = true;
			Guns[i]->isOverheat = false;
			GetWorldTimerManager().ClearTimer(Guns[i]->fireDelayTimerHandle);
			if(i == iCurrentGun)
				Particles_For_Overheat->Deactivate();
		}
}


void ARhythmShooterCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ARhythmShooterCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void ARhythmShooterCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void ARhythmShooterCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void ARhythmShooterCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ARhythmShooterCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ARhythmShooterCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ARhythmShooterCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool ARhythmShooterCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ARhythmShooterCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &ARhythmShooterCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ARhythmShooterCharacter::TouchUpdate);
		return true;
	}
	
	return false;
}

void ARhythmShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsDeath)
	{
		DeathSpringArmComponent->AddWorldRotation(FQuat(FVector(0.0f, 0.0f, 1.0f), DeltaTime * DeathCameraAngleSpeed));
		SetActorLocation(DeathMesh->GetComponentLocation());
	}
	else if (BloodWidget)
		BloodWidget->SetOpacity(1.0 - CurrentHealth / MaxHealth);
	FHitResult Hit = LineTrace();
	AfterLineTrace(Hit);
}

FHitResult ARhythmShooterCharacter::LineTrace()
{
	FHitResult OutHit;

	FVector Start = FirstPersonCameraComponent->GetComponentLocation();

	FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
	FVector End = ((ForwardVector * MaxUseDistance) + Start);
	FCollisionQueryParams CollisionParams;

	//DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);

	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams))
	{
		/*if (OutHit.bBlockingHit)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("You are hitting: %s"), *OutHit.GetActor()->GetName()));
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("Impact Point: %s"), *OutHit.ImpactPoint.ToString()));
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("Normal Point: %s"), *OutHit.ImpactNormal.ToString()));
		}*/
	}
	return OutHit;
}

void ARhythmShooterCharacter::PickUp()
{
	FHitResult Hit = LineTrace();
	APickupActor* _used = Cast<APickupActor>(Hit.GetActor());
	if (_used)
	{
		_used->OnUsed(this);
	}
}
