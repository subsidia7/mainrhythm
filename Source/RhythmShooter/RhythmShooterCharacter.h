// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HUDS/OneImageWidget.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "GameFramework/Character.h"
#include "Gun.h"
#include "CharacterProjHitInfo.h"
#include <vector>
#include "RhythmShooterCharacter.generated.h"


class UInputComponent;

UCLASS(config=Game)
class ARhythmShooterCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Mesh, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* Mesh1P;
	
	/**Full body mesh, seen after death*/
	UPROPERTY(VisibleDefaultsOnly, Category = "Death")
		class USkeletalMeshComponent* DeathMesh;

	/**Camera for death mod*/
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Death", meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* DeathCameraComponent;

	/**Spring arm for death camera*/
	UPROPERTY(VisibleDefaultsOnly,  Category = "Death")
	class USpringArmComponent* DeathSpringArmComponent;


	/** Gun mesh: 1st person view (seen only by self) */
	//?????UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** Particles for gun oveheat*/
	UPROPERTY(VisibleDefaultsOnly, Category = Particles)
		UParticleSystemComponent* Particles_For_Overheat;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* L_MotionController;

public:
	ARhythmShooterCharacter();

protected:
	virtual void BeginPlay();

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
	

//////////////////////////////////////
public:
	/** Guns */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Guns)
		TArray<TSubclassOf<class AGun>> GunsArray;
	TArray<AGun*> Guns;
	//"Mushka" for guns
	TArray <UMushkaWidget*> mushka;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Guns)
		TArray <UUserWidget*> GunsIcons;

	
	UPROPERTY(BlueprintReadOnly, Category = "Guns")
		int iCurrentGun = 0;

	UFUNCTION(BlueprintCallable)
		void SetGunWithIndex(int index);

private:
	void NextGun();
	void PrevGun();
	void SetGun();
////////////////////////////////
public:
	/**Rhythm */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Rhythm)
		class ARhythmCrosshair* Crosshair;
	UFUNCTION(BlueprintCallable)
		void SetRhythmFireState();
	UFUNCTION(BlueprintCallable)
		void SetOrdinaryFireState();
	void IncreaseCombo();
	void DecreaseCombo();
	void RhythmFire();
	void OrdinaryFire();
	bool CanFire;
	/** Rhythm combo*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rhythm)
		int Combo;
	/**Number of misses now*/
	int numberOfRitmMisses;
	/**When combo:=0*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rhythm)
		int MaxNumberOfRitmMisses;	
	void (ARhythmShooterCharacter::*FireFunction)();
	/**Music on level*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Rhythm)
		USoundCue* LevelMusic;

	/**Component that plays in the level*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Rhythm)
		UAudioComponent* LevelMusicComponent;
	UFUNCTION(BlueprintCallable)
		void SetLvlMusic(USoundCue* LvlMusic);
	UFUNCTION(BlueprintCallable)
		void SetLvlMusicComp(UAudioComponent* LvlMusic);

	/**Volume multiplication depending on combo */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rhythm)
		float VolumeMutliplier;
	/**Step of volume multiplication */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rhythm)
		float VolumeMutliplierAdding;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rhythm)
		TArray<int> ComboLimitArray;
	int LimitArrayIndex;
	bool ComboInLimitArray();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Aiming)
		bool bIsAiming;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rhythm)
		TSubclassOf<class ARhythmPerfection> RhythmPerfectionClass;
	void SpawnRhythmPerfection();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rhythm)
		FVector PerfectionDistance;
///////////////////////////////

public:
	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	//?????UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class ARhythmShooterProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	//?????UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;	

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint32 bUsingMotionControllers : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float MaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float CurrentHealth;

	/** Take damage. */
	UFUNCTION(BlueprintCallable)
		void MyTakeDamage(float damage);

	/**Blood on screen. */
	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
		TSubclassOf<UOneImageWidget> BloodWidgetClass;
	UOneImageWidget* BloodWidget;

	/**Death*/
	void Death();
	UPROPERTY(EditDefaultsOnly, Category = "Death")
		TSubclassOf<UUserWidget> DeathWidgetClass;
	UUserWidget* DeathWidget;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Death")
		bool IsDeath;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Death")
		float DeathCameraAngleSpeed;


	/**Interection with enemy*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		bool CheckMisses;
	int numberOfMisses;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		int MaxNumberOfEnemyMisses;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		int Score;
	TArray<CharacterProjHitInfo> hitInfoArray;
	CharacterProjHitInfo* FindFreeHitElement();
	void CheckHits();

protected:
	
	/** Fires a projectile. */
	void OnFire();

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	//This function allow to fire for gun. Idealy it should be in gun class)
	void AllowFire();

	void AllowFireAfterOverheat();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintImplementableEvent)
	void AfterLineTrace(FHitResult Hit);
	FHitResult LineTrace();
protected:
	/* Max distance to use/focus on actors. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float MaxUseDistance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float MaxEnemyOutlineDistance;

	/* True only in first frame when focused on new usable actor. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	bool bHasNewFocus;

	/* Actor derived from UsableActor currently in center-view. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class APickupActor* FocusedPickupActor;

public:

	/** Use the actor currently in view (if derived from UsableActor) */
	UFUNCTION(BlueprintCallable)
	void PickUp();
};

