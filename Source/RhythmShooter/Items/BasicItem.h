#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "IBasicItem.h"
#include "BasicItem.generated.h"


UCLASS()
class RHYTHMSHOOTER_API ABasicItem : public AActor, public IBasicItem
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABasicItem();
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
		class USoundBase* SpawnSound;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
		class USoundBase* OverlapSound;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
		UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Moving)
		float RotationRate;
	UFUNCTION()
		void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);
	virtual void SpawnEffect();
	class AItemSpawner* ItemSpawner;
	virtual void MakeEffect(ARhythmShooterCharacter* _Character) override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};


