// Fill out your copyright notice in the Description page of Project Settings.


#include "BasicItem.h"
#include "ItemSpawner.h"
//#include "ItemsCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABasicItem::ABasicItem()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnSound = CreateDefaultSubobject<USoundBase>("SpawnSound");
	OverlapSound = CreateDefaultSubobject<USoundBase>("OverlapSound");
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	OnActorBeginOverlap.AddDynamic(this, &ABasicItem::OnOverlap);
	ItemSpawner = nullptr;
}

// Called when the game starts or when spawned
void ABasicItem::BeginPlay()
{
	Super::BeginPlay();
}

void ABasicItem::SpawnEffect()
{
	UGameplayStatics::PlaySoundAtLocation(this, SpawnSound, GetActorLocation());
}

// Called every frame
void ABasicItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddActorLocalRotation(FRotator(0, RotationRate * DeltaTime, 0));
}

void ABasicItem::MakeEffect(ARhythmShooterCharacter* _Character)
{

}

void ABasicItem::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	ARhythmShooterCharacter* Character = Cast<ARhythmShooterCharacter>(OtherActor);
	if (Character != nullptr && ItemSpawner != nullptr)
	{
		ItemSpawner->SetTimer();
		MakeEffect(Character);
		Destroy();
		UGameplayStatics::PlaySoundAtLocation(this, OverlapSound, GetActorLocation());
	}
}