// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Items/BasicItem.h"
#include "MedKit.generated.h"

/**
 *
 */
UCLASS()
class RHYTHMSHOOTER_API AMedKit : public ABasicItem
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Healing(in percentage)")
		float HealingPower;
	/** Override function from BasicItem Interface*/
	virtual void MakeEffect(ARhythmShooterCharacter*) override;
};
