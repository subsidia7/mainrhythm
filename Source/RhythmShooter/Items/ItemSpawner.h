#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ItemSpawner.generated.h"

UCLASS()
class RHYTHMSHOOTER_API AItemSpawner : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AItemSpawner();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Moving)
		float SpawnVolume;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Moving)
		float OffsetZ;
	virtual void SpawnItem();
	FTimerHandle SpawnTimerHandle;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
		UStaticMeshComponent* StaticMesh;
	UPROPERTY(EditDefaultsOnly, Category = ItemClass)
		TSubclassOf<class ABasicItem> ItemClass;
	ABasicItem* CurrentItem;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void SetTimer();
};

