// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "PickupActor.generated.h"

/**
 * 
 */
UCLASS()
class RHYTHMSHOOTER_API APickupActor : public AStaticMeshActor
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	bool OnUsed(ACharacter* character);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	bool StartFocusItem();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	bool EndFocusItem();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DistanceToUse;
};
