// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemSpawner.h"
#include "BasicItem.h"


// Sets default values
AItemSpawner::AItemSpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	CurrentItem = nullptr;
}

// Called when the game starts or when spawned
void AItemSpawner::BeginPlay()
{
	Super::BeginPlay();
	SetTimer();
}

void AItemSpawner::SpawnItem()
{
	UWorld* const World = GetWorld();
	FVector location = GetActorLocation();
	location.Z += OffsetZ;
	CurrentItem = World->SpawnActor<ABasicItem>(ItemClass, location, GetActorRotation());
	if (CurrentItem)
	{
		CurrentItem->ItemSpawner = this;
		CurrentItem->SpawnEffect();
	}
}

void AItemSpawner::SetTimer()
{
	GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, this, &AItemSpawner::SpawnItem, SpawnVolume, false);
}


// Called every frame
void AItemSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

