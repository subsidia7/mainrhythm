// Fill out your copyright notice in the Description page of Project Settings.


#include "MedKit.h"

void AMedKit::MakeEffect(ARhythmShooterCharacter* _Character)
{
	_Character->CurrentHealth += _Character->MaxHealth * HealingPower / 100;
	if (_Character->CurrentHealth > _Character->MaxHealth)
	{
		_Character->CurrentHealth = _Character->MaxHealth;
	}
}