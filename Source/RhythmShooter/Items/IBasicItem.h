// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "RhythmShooterCharacter.h"

/**
 *
 */
class RHYTHMSHOOTER_API IBasicItem
{
public:
	void virtual MakeEffect(ARhythmShooterCharacter*) = 0;
};
