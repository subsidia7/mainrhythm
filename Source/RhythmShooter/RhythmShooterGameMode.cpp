// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "RhythmShooterGameMode.h"
#include "RhythmShooterHUD.h"
#include "RhythmShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARhythmShooterGameMode::ARhythmShooterGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ARhythmShooterHUD::StaticClass();
}
