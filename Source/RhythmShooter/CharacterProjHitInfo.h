// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TimerManager.h"

/**
 * 
 */
class RHYTHMSHOOTER_API CharacterProjHitInfo
{
public:
	CharacterProjHitInfo();
	~CharacterProjHitInfo();

	bool isHitEnemy;
	FTimerHandle hitCheckTimer;
};
