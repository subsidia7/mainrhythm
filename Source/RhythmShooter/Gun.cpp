// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Camera/CameraComponent.h"

// Sets default values
AGun::AGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);

	Particles_For_Overheat = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticlesLocation"));
	Particles_For_Overheat->SetupAttachment(FP_Gun);
	//FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));	
	canFire = true;
	isOverheat = false;
	(RateOfFire > 0) ? delayTime = 1.0f / RateOfFire : delayTime = 0.0f;	
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	canFire = true;
	Super::BeginPlay();	
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGun::SetParameters()
{
	canFire = true;
	isOverheat = false;
	(RateOfFire > 0) ? delayTime = 1.0f / RateOfFire : delayTime = 0.0f;
}

void AGun::CreateMushka(){
	
	mushkaRef = CreateWidget<UMushkaWidget>(GetWorld(), MushkaClass);
}

