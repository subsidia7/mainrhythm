// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "RhythmShooterHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "Engine.h"
#include <fstream>
#include <string>
#include "FileHelper.h"
#include "UObject/ConstructorHelpers.h"
#include <iostream>

ARhythmShooterHUD::ARhythmShooterHUD()
{
	// Set the crosshair texture
	CrosshairTex = CreateDefaultSubobject<UTexture2D>("Crosshair");
	// Set the Tape texture
	TimeTapeTex = CreateDefaultSubobject<UTexture2D>("TimeTape");
	// Set the Circle texture
	CircleTex = CreateDefaultSubobject<UTexture2D>("Circle");
}                                         

//////////////////////////////////////////////////////////
// Events

void ARhythmShooterHUD::BeginPlay()
{
	Super::BeginPlay();
	TileRadius = CircleTex->GetSizeY() / 2;
	Combo = MinComboValue;
	//float delat = CalculateDelay();
	//GetWorld()->GetTimerManager().SetTimer(NoteTimerHandle, this, &ARhythmShooterHUD::WaitForHighFrequencyNote, delat, false);
}

void ARhythmShooterHUD::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (RhythmTiles.size())
	{
		DeleteFirstTile();
		UpdateRhythmTiles(DeltaSeconds);
	}
}

void ARhythmShooterHUD::DrawHUD()
{
	Super::DrawHUD();
	ViewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
	DrawCrossHair();
	DrawTimeTape();
	DrawRhythmTiles();
	DrawText(FString::FromInt(Combo), FColor::White, 10, 10);
}

//////////////////////////////////////////////
// All about Tiles and updating

void ARhythmShooterHUD::UpdateRhythmTiles(float DeltaSeconds)
{
	for (std::list<Tile>::iterator i = RhythmTiles.begin(); i != RhythmTiles.end(); i++)
	{
		(*i).position.X -= DeltaSeconds * velocity;
	}
}

void ARhythmShooterHUD::IncreaseCombo()
{
	if (Combo != MaxComboValue)
		Combo++;
}

void ARhythmShooterHUD::DecreaseCombo()
{
	Combo = MinComboValue;
}

void ARhythmShooterHUD::DeleteFirstTile()
{
	float LeftX = (*RhythmTiles.begin()).position.X + CircleTex->GetSizeX() / 2 - TileRadius;
	float CenterX = ViewportSize.X * 0.5f;
	float RightX = LeftX + 2 * TileRadius;
	if (RightX < CenterX)
	{
		RhythmTiles.pop_front();
		DecreaseCombo();
	}
}

void ARhythmShooterHUD::SpawnNewTile(float x)
{
	Tile newTile;
	newTile.isPressed = false;
	newTile.position.X = x;
	//newTile.position.X = ViewportSize.X * 0.5f + TimeTapeTex->GetSizeX() / 2 - CircleTex->GetSizeX() / 2;
	newTile.position.Y = ViewportSize.Y * 0.01f + TimeTapeTex->GetSizeY() / 2 - TileRadius;
	RhythmTiles.push_back(newTile);
	int b = RhythmTiles.size();
	std::cout << b;
}

// Return true if last unpressed Circle intersects with center of Tape
bool ARhythmShooterHUD::InBounds()
{
	float LeftX = (*RhythmTiles.begin()).position.X + CircleTex->GetSizeX() / 2 - TileRadius;
	float CenterX = ViewportSize.X * 0.5f;
	float RightX = LeftX + 2 * TileRadius;
	return LeftX <= CenterX && CenterX <= RightX;
}

void ARhythmShooterHUD::ChangeCombo()
{
	// If first Tile isn't pressed and intersects with center of Tape: increase Combo
	if (!(*RhythmTiles.begin()).isPressed && InBounds())
	{
		IncreaseCombo();
		(*RhythmTiles.begin()).isPressed = true;
		RhythmTiles.pop_front();
	}
	else
	{
		DecreaseCombo();
	}
}

//////////////////////////////////////////////////////////////////
// Draw Functions

void ARhythmShooterHUD::DrawCrossHair()
{
	// Draw very simple crosshair
	// find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);
	// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
	const FVector2D CrosshairDrawPosition((Center.X),
		(Center.Y + 20.0f));

	// draw the crosshair
	FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);
}

void ARhythmShooterHUD::DrawTimeTape()
{
	// Draw a tape for tiles
	const FVector2D position(ViewportSize.X * 0.5f - TimeTapeTex->GetSurfaceWidth() / 2, ViewportSize.Y * 0.01f);
	FCanvasTileItem TileItem(position, TimeTapeTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);
}

void ARhythmShooterHUD::DrawRhythmTiles()
{
	for (std::list<Tile>::iterator i = RhythmTiles.begin(); i != RhythmTiles.end(); i++)
	{
		// If the X position of tile is more then tape right corner X, do not draw Tile
		if (ViewportSize.X * 0.5f + TimeTapeTex->GetSizeX() / 2 - CircleTex->GetSizeX() / 2 < (*i).position.X)
			continue;
		FCanvasTileItem TileItem((*i).position, CircleTex->Resource, FLinearColor::White);
		TileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(TileItem);
	}
}

//////////////////////////////////////////////////////////////////////
// High Frequency Notes process functions

bool ARhythmShooterHUD::loadTimeLine(FString fileName)
{
	FString result;
	std::string full_path = TCHAR_TO_UTF8(*(FPaths::ProjectDir() + "Data/" + fileName));
	std::ifstream fin(full_path);
	if (!fin.is_open())
		return false;
	float time;
	while (fin >> time)
	{
		TimeLine.push_back(time);
		SpawnNewTile(ViewportSize.X * 0.5f + time * velocity + TileRadius);
	}
	return true;
}

/** UNUSED FOR CURRENT ALGORYTHM*/

float ARhythmShooterHUD::CalculateDelay()
{
	// Move to next High Frequency Note
	TimeLinePointer++;
	return TimeLine[TimeLinePointer] - TimeLine[TimeLinePointer - 1];
}

void ARhythmShooterHUD::WaitForHighFrequencyNote()
{
	if (TimeLinePointer == TimeLine.size())
	{
		GetWorld()->GetTimerManager().ClearTimer(NoteTimerHandle);
		return;
	}
	float delay = CalculateDelay();
	GetWorld()->GetTimerManager().SetTimer(NoteTimerHandle, this, &ARhythmShooterHUD::WaitForHighFrequencyNote, delay, false);
	SpawnNewTile(10);
} 

