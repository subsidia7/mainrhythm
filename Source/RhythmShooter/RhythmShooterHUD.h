// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include <list>
#include <vector>
#include "RhythmShooterHUD.generated.h"

/** Tile struct*/
struct Tile
{
	FVector2D position;
	bool isPressed;
};

UCLASS()
class ARhythmShooterHUD : public AHUD
{
GENERATED_BODY()

public:
	ARhythmShooterHUD();
	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;
	/** Crosshair asset pointer */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UTexture2D* CrosshairTex;
	/** Time tape asset pointer */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UTexture2D* TimeTapeTex;
	/** Tile asset pointer */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UTexture2D* CircleTex;
	/** Velocity of rhythm tiles*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float velocity;
	/** Increase Decrease combo after button press*/
	UFUNCTION(BlueprintCallable)
		void ChangeCombo();
	/** Fill timeLine from file*/
	UFUNCTION(BlueprintCallable)
		bool loadTimeLine(FString fileName);
	/** Needs to wait until the next high frequency note*/
	UFUNCTION(BlueprintCallable)
		void WaitForHighFrequencyNote();
	/** Needs to calculate time between next and previous high frequency notes*/
	UFUNCTION(BlueprintCallable)
		float CalculateDelay();
	/** The min value of combo*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int MaxComboValue;
	/** The max value of combo*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int MinComboValue;
private:
	void DrawCrossHair();
	void DrawTimeTape();
	void DrawRhythmTiles();
	void SpawnNewTile(float x);
	void UpdateRhythmTiles(float DeltaSeconds);
	bool InBounds();
	void DecreaseCombo();
	void IncreaseCombo();
	void DeleteFirstTile();
	virtual void Tick(float);
	virtual void BeginPlay();
	/** List with Tiles*/
	std::list<Tile> RhythmTiles;
	FVector2D ViewportSize;
	float TileRadius;
	int Combo = 1;
	/** Time Line (will be loaded from data file)*/
	std::vector<float> TimeLine;
	int TimeLinePointer = 0;
	/** Handle to manage the timer */
	FTimerHandle NoteTimerHandle;
};

