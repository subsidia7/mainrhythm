// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "RhythmShooterProjectile.h"
#include "RhythmShooterCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"
//#include "Engine/Engine.h" //for GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TestHUDString);
#include "Components/SphereComponent.h"

ARhythmShooterProjectile::ARhythmShooterProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ARhythmShooterProjectile::OnHit);		// set up a notification for when this component hits something blocking
	
	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 1.5f;

	// Init Mesh
	//Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	//Mesh->SetupAttachment(CollisionComp);
}

void ARhythmShooterProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	/*if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

		Destroy();
	}*/
	FVector velocity = ProjectileMovement->Velocity;
	ABasicEnemy* collidingEnemy = Cast<ABasicEnemy>(OtherActor);
	ARhythmShooterCharacter* collidingCharacter = Cast<ARhythmShooterCharacter>(OtherActor);
	
	if (collidingEnemy)
	{
		collidingEnemy->GetDamage(damage);
		collidingEnemy->ChangeMaterialState();

		--throughDamage;

		if (ARhythmShooterCharacter* Player = Cast<ARhythmShooterCharacter>(owner))
		{
			SetHitInfo();
			int points = (collidingEnemy->currentHealth <= 0) ? (collidingEnemy->currentHealth + damage) : damage;
			Player->Score += (points < 0) ? 0 : points;
		}
	}
	else{ 
		if (collidingCharacter)
		{
			collidingCharacter->MyTakeDamage(damage);
		}
		Destroy();
		return;		
	}
		
	
	if (throughDamage >= 0)
	{
		bool found = false;
		FVector exitLocation = CalculateExitLocation(OtherActor, Hit, &found);
		
		if (found)
		{
			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

			// spawn the projectile at the muzzle
			ARhythmShooterProjectile* newProjectile = GetWorld()->SpawnActor<ARhythmShooterProjectile>(ProjectileClass, exitLocation, velocity.Rotation(), ActorSpawnParams);
			
			if (newProjectile)
			{
				newProjectile->SetFireParameters(damage, ProjectileMovement->Velocity.Size(), throughDamage);
				newProjectile->ProjectileMovement->Velocity = velocity;
				newProjectile->ProjectileClass = ProjectileClass;
				newProjectile->owner = owner;
			}
			Destroy();	
		}
		else
		{
			Destroy();
		}
	}
	else
	{	
		Destroy();
	}	
}

FVector ARhythmShooterProjectile::CalculateExitLocation(AActor* OtherActor, const FHitResult Hit, bool* isFound)
{
		
	FVector start = Hit.Location + ProjectileMovement->Velocity.GetSafeNormal()*500.0f;
	FVector end = Hit.Location;
	float radius = CollisionComp->GetScaledSphereRadius();
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_WorldStatic));
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_WorldDynamic));
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_Pawn));
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_PhysicsBody));
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_Destructible));

	TArray< AActor * > ActorsToIgnore;	
	TArray< FHitResult > OutHits;
	UKismetSystemLibrary::CapsuleTraceMultiForObjects(GetWorld(), start, end, radius, radius,
		ObjectTypes, false, ActorsToIgnore, EDrawDebugTrace::Type::None, OutHits, true);

	for (int i = 0; i < OutHits.Num(); i++)
	{
		if (OutHits[i].Actor == OtherActor)
		{
			*isFound = true;
			return OutHits[i].Location;
		}
	}
	
	*isFound = false;
	return FVector();
}

void ARhythmShooterProjectile::SetFireParameters(float newdamage, float speed, int newThroughDamage)
{
	damage = newdamage;
	SetActualAndMaxSpeed(speed, speed);
	throughDamage = newThroughDamage;
}

void ARhythmShooterProjectile::SetActualAndMaxSpeed(float actualSpeed, float maxSpeed)
{
	ProjectileMovement->MaxSpeed = maxSpeed;
	ProjectileMovement->Velocity = ProjectileMovement->Velocity.GetSafeNormal() * actualSpeed;
}

void ARhythmShooterProjectile::SetHitInfo()
{
	ARhythmShooterCharacter* _Owner = Cast<ARhythmShooterCharacter>(owner);
	if(hitInfo && _Owner)
		if (!hitInfo->isHitEnemy)
		{
			hitInfo->isHitEnemy = true;
			_Owner->numberOfMisses = 0;			
		}
}
