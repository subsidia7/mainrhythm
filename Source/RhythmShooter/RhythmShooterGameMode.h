// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RhythmShooterGameMode.generated.h"

UCLASS(minimalapi)
class ARhythmShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARhythmShooterGameMode();
};



