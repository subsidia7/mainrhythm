// Fill out your copyright notice in the Description page of Project Settings.


#include "RhythmCharacter.h"
#include "SRhythm.h"

// Sets default values
ARhythmCharacter::ARhythmCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARhythmCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARhythmCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARhythmCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ARhythmCharacter::MakeEffect()
{
}
