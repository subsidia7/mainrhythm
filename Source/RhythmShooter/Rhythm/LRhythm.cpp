// Fill out your copyright notice in the Description page of Project Settings.


#include "LRhythm.h"
#include <string>
#include <fstream>
#include "HUDS/RhythmCrosshair.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "Components/AudioComponent.h"


UAudioComponent* ALRhythm::CurrentSound;

ALRhythm::ALRhythm()
{
	PrimaryActorTick.bCanEverTick = true;

	MainMusic = CreateDefaultSubobject<UAudioComponent>(TEXT("MainMusic"));

	LoopMusic = CreateDefaultSubobject<UAudioComponent>(TEXT("LoopMusic"));

	EndMusic = CreateDefaultSubobject<UAudioComponent>(TEXT("EndMusic"));
}

void ALRhythm::BeginPlay()
{
	Super::BeginPlay();
	LoadTimeLine();
	state = STOP;
}

void ALRhythm::ClearData()
{
	MusicInfo.MainData.Empty();
	MusicInfo.LoopData.Empty();
	MusicInfo.MainEndData.Empty();
	MusicInfo.LoopEndData.Empty();
}

bool Load(std::string path, TArray<FSTimeCode>& data)
{
	FSTimeCode timeCode;
	std::ifstream fin;
	fin.open(path);
	if (!fin.is_open())
		return false;
	while (fin >> timeCode.time)
	{
		data.Add(timeCode);
	}
	return true;
}


bool ALRhythm::LoadTimeLine()
{
	ALRhythm::ClearData();
	std::string dir_path = TCHAR_TO_UTF8(*(FPaths::ProjectDir() + "Content/Data/" + Directory));

	if (!Load(dir_path + "/main.txt", MusicInfo.MainData))
		return false;
	if (!Load(dir_path + "/loop.txt", MusicInfo.LoopData))
		return false;
	if (!Load(dir_path + "/main_end.txt", MusicInfo.MainEndData))
		return false;
	if (!Load(dir_path + "/loop_end.txt", MusicInfo.LoopEndData))
		return false;

	return true;
}

void ALRhythm::PauseSound()
{
	CurrentSound->SetPaused(true);
}

void ALRhythm::UnpauseSound()
{
	CurrentSound->SetPaused(false);
}

void ALRhythm::Main()
{
	float time = World->GetAudioTimeSeconds() - MusicInfo.StartRhythmTime;
	float diff = MusicInfo.MainData[MusicInfo.iMain].time - time;

	if (time >= MusicInfo.MainData[MusicInfo.iMain].time - MusicInfo.Crosshair->TimeToReachTarget)
	{
		MusicInfo.Crosshair->AddNote();
		UE_LOG(LogTemp, Warning, TEXT("MAAAAIN time %f NOTA %f DIFF %f"), time, MusicInfo.MainData[MusicInfo.iMain].time, diff);
		MusicInfo.iMain++;

	}
	if (MusicInfo.iMain == MusicInfo.MainData.Num())
	{
		float Dur = CurrentSound->Sound->GetDuration();
		float timeSeconds = World->GetAudioTimeSeconds();
		float startTime = MusicInfo.StartRhythmTime;

		float curTIme = (timeSeconds - startTime);
		float DelayStart = Dur - curTIme;
		MusicInfo.iLoop = 0;
		float DelayNote = Dur + MusicInfo.LoopData[MusicInfo.iLoop].time - MusicInfo.Crosshair->TimeToReachTarget - curTIme;
		UE_LOG(LogTemp, Warning, TEXT("MAAAAIN DelayStart: %f DelayNote: %f Dur: %f curTIme: %f startTime: %f timeSeconds: %f"), DelayStart, DelayNote, Dur, curTIme, startTime, timeSeconds);
		
		World->GetTimerManager().SetTimer(StartHandle, this, &ALRhythm::StartPlayLoop, DelayStart, false);
		World->GetTimerManager().SetTimer(TimerHandle, this, &ALRhythm::TimerLoop, DelayNote, false);

		state = STOP;
	}
}

void ALRhythm::Loop()
{
	float time = World->GetAudioTimeSeconds() - MusicInfo.StartRhythmTime;
	float diff = MusicInfo.LoopData[MusicInfo.iLoop].time - time;

	if (time >= MusicInfo.LoopData[MusicInfo.iLoop].time - MusicInfo.Crosshair->TimeToReachTarget)
	{
		MusicInfo.Crosshair->AddNote();
		UE_LOG(LogTemp, Warning, TEXT("LOOOOP time %f NOTA %f DIFF %f"), time, MusicInfo.LoopData[MusicInfo.iLoop].time, diff);
		MusicInfo.iLoop++;

	}
	if (MusicInfo.iLoop == MusicInfo.LoopData.Num())
	{
		float Dur = CurrentSound->Sound->GetDuration();
		float timeSeconds = World->GetAudioTimeSeconds();
		float startTime = MusicInfo.StartRhythmTime;

		float curTIme = (timeSeconds - startTime);
		float DelayStart = Dur - curTIme;
		MusicInfo.iLoop = 0;
		float DelayNote = Dur + MusicInfo.LoopData[MusicInfo.iLoop].time - MusicInfo.Crosshair->TimeToReachTarget - curTIme;
		UE_LOG(LogTemp, Warning, TEXT("LOOOOP Delay: %f DelayNote: %f Dur: %f curTIme: %f startTime: %f timeSeconds: %f"), DelayStart, DelayNote, Dur, curTIme, startTime, timeSeconds);

		World->GetTimerManager().SetTimer(StartHandle, this, &ALRhythm::StartPlayLoop, DelayStart, false);
		World->GetTimerManager().SetTimer(TimerHandle, this, &ALRhythm::TimerLoop, DelayNote, false);

		state = STOP;
	}

}

void ALRhythm::Tick(float dt)
{
	Super::Tick(dt);
	/*{
		float rofl = World->GetAudioTimeSeconds() - MusicInfo.StartRhythmTime;
		UE_LOG(LogTemp, Warning, TEXT("Start TimerMain %f NOTA %f DIFF %f"), rofl, MusicInfo.MainData[MusicInfo.iMain].time, diff);
	}*/
	switch (state)
	{
	case STOP:
		break;
	case MAIN:
		Main();
		break;
	case LOOP:
		Loop();
		break;
	default:
		break;
	}

}

void ALRhythm::StartRhythm(ARhythmCrosshair* _Crosshair)
{
	CurrentSound = nullptr;
	World = GetWorld();
	MusicInfo.Crosshair = _Crosshair;
	MusicInfo.iMain = 0;
	if (MainMusic->Sound && MusicInfo.MainData.Num() != 0)
	{
		float FirstNoteDelay = MusicInfo.MainData[0].time - MusicInfo.Crosshair->TimeToReachTarget;
		if (FirstNoteDelay >= 0)
		{
			StartPlayMain();
			World->GetTimerManager().SetTimer(TimerHandle, this, &ALRhythm::TimerMain, FirstNoteDelay, false);
		}
		else
		{
			World->GetTimerManager().SetTimer(StartHandle, this, &ALRhythm::StartPlayMain, abs(FirstNoteDelay), false);
			
		}
	}
	else if (LoopMusic->Sound && MusicInfo.LoopData.Num() != 0)
	{
		float FirstNoteDelay = MusicInfo.LoopData[0].time - MusicInfo.Crosshair->TimeToReachTarget;
		if (FirstNoteDelay >= 0)
		{
			StartPlayLoop();
			World->GetTimerManager().SetTimer(TimerHandle, this, &ALRhythm::TimerLoop, FirstNoteDelay, false);
		}
		else
		{
			World->GetTimerManager().SetTimer(StartHandle, this, &ALRhythm::StartPlayLoop, abs(FirstNoteDelay), false);
			TimerLoop();
		}
	}
}

void ALRhythm::StartPlayMain()
{
	CurrentSound = MainMusic;
	MusicInfo.MusicTime = 0;
	MusicInfo.Crosshair->StartRhythm();
	if (CurrentSound->Sound)
		CurrentSound->Play();
	state = MAIN;
	MusicInfo.StartRhythmTime = World->GetAudioTimeSeconds();
}

void ALRhythm::StartPlayLoop()
{
	if(CurrentSound)
		CurrentSound->FadeOut(0.0f, 0.0f);
	CurrentSound = LoopMusic;
	MusicInfo.MusicTime = 0;
	if (CurrentSound->Sound)
		CurrentSound->FadeIn(0.1f);
	state = LOOP;
	MusicInfo.StartRhythmTime = World->GetAudioTimeSeconds();
}

void ALRhythm::TimerLoop()
{
	MusicInfo.Crosshair->AddNote();
	MusicInfo.iLoop++;
}

void ALRhythm::TimerMain()
{
	MusicInfo.Crosshair->AddNote();
	MusicInfo.iMain++;
}

void ALRhythm::EndRhythm()
{
	World->GetTimerManager().ClearTimer(TimerHandle);
	World->GetTimerManager().ClearTimer(StartHandle);
	MusicInfo.Crosshair->EndRhythm();
	state = STOP;
	if (EndMusic->Sound)
	{
		CurrentSound->FadeOut(0.0f, 0.0f);
		CurrentSound = EndMusic;
		CurrentSound->FadeIn(0.0f);
		World->GetTimerManager().SetTimer(TimerHandle, this, &ALRhythm::Des, EndMusic->Sound->GetDuration() + 0.5f, false);
	}
	else
	{
		CurrentSound->FadeOut(1.0f, 0.0f);
	}
}

void ALRhythm::Des()
{
	Destroy();
}


