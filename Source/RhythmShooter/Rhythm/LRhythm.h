// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LRhythm.generated.h"


USTRUCT(BlueprintType)
struct FSTimeCode
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite)
	float time;
};

USTRUCT(BlueprintType)
struct FSMusicInfo
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
		TArray<FSTimeCode> MainData;
	
	UPROPERTY(BlueprintReadWrite)
		TArray<FSTimeCode> LoopData;
	
	UPROPERTY(BlueprintReadWrite)
		TArray<FSTimeCode> MainEndData;
	
	UPROPERTY(BlueprintReadWrite)
		TArray<FSTimeCode> LoopEndData;

	UPROPERTY(BlueprintReadWrite)
		float MusicTime;

	UPROPERTY(BlueprintReadWrite)
		float StartRhythmTime;

	UPROPERTY(BlueprintReadWrite)
		float TimeToReachTarget;

	int iMain = 0;
	int iLoop = 0;

	class ARhythmCrosshair* Crosshair;
};

/**
 * 
 */
enum States { MAIN, LOOP, END, STOP };

UCLASS()
class RHYTHMSHOOTER_API ALRhythm : public AActor
{
	GENERATED_BODY()
public:
	ALRhythm();
	/** Primary draw call for the HUD */
	virtual void BeginPlay() override;
	virtual void Tick(float) override;

	UFUNCTION(BlueprintCallable)
		bool LoadTimeLine();
	
	UFUNCTION(BlueprintCallable)
		void ClearData();

	UFUNCTION(BlueprintCallable)
		void StartRhythm(class ARhythmCrosshair* _Crosshair);

	UFUNCTION(BlueprintCallable)
		void EndRhythm();

	UFUNCTION(BlueprintCallable)
		static void PauseSound();

	UFUNCTION(BlueprintCallable)
		static void UnpauseSound();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UAudioComponent* MainMusic;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UAudioComponent* LoopMusic;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UAudioComponent* EndMusic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Directory;
private:
	static class UAudioComponent* CurrentSound;
	FSMusicInfo MusicInfo;
	void StartPlayMain();
	void StartPlayLoop();
	void TimerMain();
	void TimerLoop();
	void MakeEnd();
	UWorld* World;
	FTimerHandle TimerHandle;
	FTimerHandle StartHandle;

	TArray<float> NoteTimes;
	TArray<float> CurTimes;
	bool bRhythm = false;
	States state;

	void Main();
	void Loop();
	void Des();
	float endTime;
};
