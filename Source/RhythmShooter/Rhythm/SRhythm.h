//// Fill out your copyright notice in the Description page of Project Settings.
//
//#pragma once
//
//#include "CoreMinimal.h"
//#include "UObject/NoExportTypes.h"
//#include <vector>
//#include "SRhythm.generated.h"
//
///**
// * 
// */
//USTRUCT(BlueprintType)
//struct FRTimeCode
//{
//	GENERATED_BODY()
//	UPROPERTY(BlueprintReadWrite)
//	float time;
//};
//
//
//UCLASS()
//class RHYTHMSHOOTER_API USRhythm : public UObject
//{
//	GENERATED_BODY()
//private:
//	
//	static USRhythm* pInstance;
//	TArray<FRTimeCode> Data;
//	void AddTimeCode(FRTimeCode);
//	//std::vector<FRTimeCode> Data;
//
//public:
//	USRhythm();
//	UFUNCTION(BlueprintCallable)
//	static USRhythm* GetInstance()
//	{
//		if (!pInstance)
//		{
//			pInstance = NewObject<USRhythm>();
//		}
//		return pInstance;
//	}
//	UFUNCTION(BlueprintCallable)
//	static TArray<FRTimeCode> GetData();
//	UFUNCTION(BlueprintCallable)
//	static bool LoadTimeLine(FString fileName);
//
//};
