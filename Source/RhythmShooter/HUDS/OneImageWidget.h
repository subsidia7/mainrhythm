// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "OneImageWidget.generated.h"

/**
 * 
 */
UCLASS()
class RHYTHMSHOOTER_API UOneImageWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UOneImageWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	void Show();
	void Hide();

	void SetOpacity(double newOpacity);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		UImage* main_image;
};
