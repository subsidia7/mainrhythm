// Fill out your copyright notice in the Description page of Project Settings.


#include "MushkaWidget.h"

UMushkaWidget::UMushkaWidget(const FObjectInitializer& ObjectInitializer):Super(ObjectInitializer)
{
}

void UMushkaWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UMushkaWidget::Show()
{
	mushka->SetVisibility(ESlateVisibility::Visible);
	this->AddToViewport();
}

void UMushkaWidget::Hide()
{
	this->RemoveFromParent();
}