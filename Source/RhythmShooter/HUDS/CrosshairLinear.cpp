// Fill out your copyright notice in the Description page of Project Settings.


#include "CrosshairLinear.h"
#include "Engine/Canvas.h"
#include "TextureResource.h"
#include "Engine.h"
#include "Engine/Texture.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/KismetMathLibrary.h"

UTexture* Note::Texture = 0;

ACrosshairLinear::ACrosshairLinear()
{
	NoteTex = CreateDefaultSubobject<UTexture>("NoteTex");
	TargetTex = CreateDefaultSubobject<UTexture>("TargetTex");
	
}

void ACrosshairLinear::BeginPlay()
{
	Super::BeginPlay();
	// Note setup
	Note::Texture = NoteTex;
	NoteWidth = Note::Texture->Resource->GetSizeX();
	NoteHeight = Note::Texture->Resource->GetSizeY();
	NoteWidthHalf = NoteWidth * 0.5f;
	NoteHeightHalf = NoteHeight * 0.5f;
	// Target setup
	TargetWidth = TargetTex->Resource->GetSizeX();
	TargetHeight = TargetTex->Resource->GetSizeY();
	TargetHalfHeight = TargetHeight * 0.5f;
	TargetHalfWidth = TargetWidth * 0.5f;
	// Distance of each perfection zone
	Dispersion = (float)TargetHalfHeight / PerfectionStates.Num();
	// ScalingVelocity after reaching target
	ScaleVelocityAfterDestination = 1.0f / (NoteHeight / Velocity);
}

/////////////////////////////////////////////////////////////
///////// DRAWING
void ACrosshairLinear::DrawHUD()
{
	Super::DrawHUD();
	// set viewport center position
	ViewportCenter.X = Canvas->ClipX * 0.5f;
	ViewportCenter.Y = Canvas->ClipY * 0.5f;
	// set start position
	StartPosition.X = ViewportCenter.X - NoteWidthHalf * StartScale;
	StartPosition.Y = Canvas->ClipY * 0.8f - NoteHeight * StartScale;
	// set target position
	TargetPosition.X = ViewportCenter.X - TargetHalfWidth;
	TargetPosition.Y = ViewportCenter.Y - TargetHalfHeight;
	// distance between start and target
	DistanceFromStartToTarget = StartPosition.Y - TargetPosition.Y;
	TimeToReachTarget = (float) DistanceFromStartToTarget / Velocity;
	ScaleVelocity = (float)(1.0 - StartScale) / TimeToReachTarget;
	// drawing all
	DrawTarget();
	DrawNotes();
}

void ACrosshairLinear::DrawTarget()
{
	// draw the target
	FCanvasTileItem TileItem(TargetPosition, TargetTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);
}

void ACrosshairLinear::DrawNotes()
{
	FCanvasTileItem TileItem(FVector2D(0, 0), Note::Texture->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	for (std::list<Note>::iterator i = RhythmCircles.begin(); i != RhythmCircles.end(); i++)
	{
		TileItem.Position = i->Position;
		TileItem.Size = FVector2D(i->CurrentWidth, i->CurrentHeight);
		Canvas->DrawItem(TileItem);
	}
}

/////////////////////////////////////////////////////////////
//////////////////// UPDATING
void ACrosshairLinear::Tick(float dt)
{
	Super::Tick(dt);
	if (!RhythmCircles.empty())
	{
		UpdateCircles(dt);
		DeleteNote();
	}
}

void ACrosshairLinear::UpdateCircles(float dt)
{
	for (std::list<Note>::iterator i = RhythmCircles.begin(); i != RhythmCircles.end(); i++)
	{
		//i->position = FMath::Vector2DInterpTo(i->position, TargetPosition, dt, Velocity);
		if (!i->IsReachedTarget && i->Scale < 1.f)
		{
			i->Scale += ScaleVelocity * dt;
		}
		else
		{
			i->Scale -= ScaleVelocityAfterDestination * dt;
		}
		i->CurrentWidth = i->Scale * NoteWidth;
		i->CurrentHeight = i->Scale * NoteHeight;
		i->Position.X = ViewportCenter.X - i->CurrentWidth / 2.f;
		if (i->Position.Y <= TargetPosition.Y)
		{
			i->IsReachedTarget = true;
		}
		else if(!i->IsReachedTarget)
		{
			i->Position.Y -= Velocity * dt;
		}
	}
}

//////////////////////////////////////////////////////////////
void ACrosshairLinear::StartRhythm()
{
	
}

void ACrosshairLinear::EndRhythm()
{
	RhythmCircles.clear();
}

bool ACrosshairLinear::InRhythmPressed()
{
	if (RhythmCircles.empty())
	{
		PerfectionIndex = PerfectionStates.Num() - 1;
		return false;
	}
	PerfectionIndex = CalculatePerfectionStateIndex();
	RhythmCircles.pop_front();
	if(PerfectionIndex != PerfectionStates.Num() - 1)
		return true;
	else 
		return false;
}

void ACrosshairLinear::AddNote()
{
	Note note;
	note.Position = StartPosition;
	note.Scale = StartScale;
	note.CurrentWidth = NoteWidth * StartScale;
	note.CurrentHeight = NoteHeight * StartScale;
	note.IsReachedTarget = false;
	RhythmCircles.push_back(note);
}

void ACrosshairLinear::DeleteNote()
{
	if (RhythmCircles.begin()->IsReachedTarget && RhythmCircles.begin()->Scale <= 0)
	{
		RhythmCircles.pop_front();
	}
}

int ACrosshairLinear::CalculatePerfectionStateIndex()
{
	FVector2D TargetCenter(TargetPosition.X + TargetHalfWidth, TargetPosition.Y + TargetHalfHeight);
	FVector2D NoteCenter(RhythmCircles.begin()->Position.X + RhythmCircles.begin()->CurrentWidth / 2, RhythmCircles.begin()->Position.Y + RhythmCircles.begin()->CurrentHeight / 2);
	int index = FVector2D::Distance(TargetCenter, NoteCenter) / Dispersion;
	
	/***************
		UKismetSystemLibrary::PrintString(GetWorld(), TargetPosition.ToString());
		UKismetSystemLibrary::PrintString(GetWorld(), ViewportCenter.ToString());
		UKismetSystemLibrary::PrintString(GetWorld(), TargetCenter.ToString());
		UKismetSystemLibrary::PrintString(GetWorld(), NoteCenter.ToString());
		UKismetSystemLibrary::PrintString(GetWorld(), FString::SanitizeFloat(Dispersion));
		UKismetSystemLibrary::PrintString(GetWorld(), FString::FromInt((FVector2D::Distance(TargetCenter, NoteCenter))));
		UKismetSystemLibrary::PrintString(GetWorld(), FString::FromInt(PerfectionIndex));
		UKismetSystemLibrary::PrintString(GetWorld(), ViewportCenter.ToString());
		UKismetSystemLibrary::PrintString(GetWorld(), NoteCenter.ToString());
	****************/
	if (index > PerfectionStates.Num() - 1 || NoteCenter.Y <= TargetCenter.Y - TargetHalfHeight)
	{
		index = PerfectionStates.Num() - 1;
	}
	return index;
}



