// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RhythmPerfection.generated.h"

UCLASS()
class RHYTHMSHOOTER_API ARhythmPerfection : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARhythmPerfection();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UWidgetComponent* PerfectionWidget;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class UPerfectionWidget> WidgetClass;

	void SetPlayer(class ARhythmShooterCharacter* Player);
	
	void SetTextAndColor(struct FPerfection&);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Velocity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Offset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float TimeToDestroy;
private:
	class ARhythmShooterCharacter* Player;
	FTimerHandle Timer;
	void Destroying();
};
