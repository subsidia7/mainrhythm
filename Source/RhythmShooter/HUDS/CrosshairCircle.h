// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include <list>
#include "Rhythm/LRhythm.h"
#include "RhythmCrosshair.h"
#include "CrosshairCircle.generated.h"

/**
 * 
 */

struct Circle
{
	FVector2D position;
	class UTexture* Texture;
	float Velocity;
	float DeflectAngle;
	bool isPressed;
	bool isDrawable;
};

UCLASS()
class RHYTHMSHOOTER_API ACrosshairCircle : public ARhythmCrosshair
{
	GENERATED_BODY()
public:
	ACrosshairCircle();
	/** Primary draw call for the HUD */
	virtual void BeginPlay() override;
	virtual void DrawHUD() override;
	virtual void Tick(float) override;
	/** StartRhythm*/
	virtual void StartRhythm() override;
	virtual void EndRhythm() override;
	/** Crosshair asset pointer */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Textures")
		class UTexture* CrosshairTex;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Textures")
		class UTexture* HintTex;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Textures")
		class UTexture* StandartCircleTex;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Textures")
		class UTexture* CanPressedCircleTex;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Textures")
		class UTexture* LostCircleTex;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Textures")
		class UTexture* TargetTex;
	/** Velocity of rhythm tiles*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Angle Velocity")
		float StandartCircleVelocity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Angle Velocity")
		float LostCircleVelocity;
	// rhythm
	/*UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Gameplay)
		int Combo;*/
	/** Check for rhythm fire*/
	virtual bool InRhythmPressed() override;
private:
	/*void DecreaseCombo();
	void IncreaseCombo();*/
	// collision
	bool TargetAndCircleCollides();
	// draw
	void DrawHint();
	void DrawCircles();
	void DrawTargetTex();
	void DrawCrosshair();
	// update Circles
	void UpdateCirclePosition(float);
	void AddCircle(/*float DefAngle*/);
	void DeleteCircle();
	void MakeCircleLost();
	void MakeCanPressedCircle();
	// values
	float HintRadius;
	float CircleRadius;
	float HalfOfHintWidth;
	float HalfOfHintHeight;
	float DeflectAngle;
	// positions
	FVector2D ViewportCenter;
	FVector2D HintPosition;
	FVector2D CrosshairPosition;
	//FVector2D CirclePosition;
	FVector2D TargetPosition;
	// rhythm list of circles
	std::list<Circle> RhythmCircles;
	// time handler
	FTimerHandle TimeHandle;
	float Period;
	float DelayTime;
	void StartAdding();
	//USRhythm* Rhythm;

	virtual int CalculatePerfectionStateIndex() override;

};
