// Fill out your copyright notice in the Description page of Project Settings.


#include "CrosshairCircle.h"
#include "Engine/Canvas.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "Engine.h"
#include "Engine/Texture.h"
#include "UObject/ConstructorHelpers.h"

#define Def_ANGLE -PI / 2

ACrosshairCircle::ACrosshairCircle()
{
	CrosshairTex = CreateDefaultSubobject<UTexture>("CrosshairTex");
	HintTex = CreateDefaultSubobject<UTexture>("HintTex");
	StandartCircleTex = CreateDefaultSubobject<UTexture>("StandartCircleTex");
	CanPressedCircleTex = CreateDefaultSubobject<UTexture>("CanPressedCircleTex");
	LostCircleTex = CreateDefaultSubobject<UTexture>("LostCircleTex");
}

void ACrosshairCircle::BeginPlay()
{
	Super::BeginPlay();
	//Rhythm = Rhythm->GetInstance();
	Period = 2 * PI / StandartCircleVelocity;
	DeflectAngle = Def_ANGLE;
	HalfOfHintWidth = HintTex->Resource->GetSizeX() / 2;
	HalfOfHintHeight = HintTex->Resource->GetSizeY() / 2;
	HintRadius = HalfOfHintHeight;
	CircleRadius = StandartCircleTex->Resource->GetSizeX() / 2;
}

/////////////////////////////////////////// DRAWING
void ACrosshairCircle::DrawHUD()
{
	Super::DrawHUD();
	// set center screen pos
	ViewportCenter.X = Canvas->ClipX * 0.5f;
	ViewportCenter.Y = Canvas->ClipY * 0.5f;
	// set hint pos
	HintPosition.X = ViewportCenter.X - HintRadius;
	HintPosition.Y = ViewportCenter.Y - HintRadius;
	// set target position
	TargetPosition.X = ViewportCenter.X - TargetTex->Resource->GetSizeX() / 2;
	TargetPosition.Y = ViewportCenter.Y - HintRadius - TargetTex->Resource->GetSizeX() / 2;
	// set crosshair position
	CrosshairPosition.X = ViewportCenter.X - CrosshairTex->Resource->GetSizeX() / 2;
	CrosshairPosition.Y = ViewportCenter.Y - CrosshairTex->Resource->GetSizeY() / 2;
	// draw all
	DrawHint();
	DrawTargetTex();
	DrawCircles();
	//DrawCrosshair();
}

void ACrosshairCircle::DrawHint()
{
	// draw the hint
	FCanvasTileItem TileItem(HintPosition, HintTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);
}

void ACrosshairCircle::DrawTargetTex()
{
	// draw the target circle
	FCanvasTileItem TileItem(TargetPosition, TargetTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);
}

void ACrosshairCircle::DrawCircles()
{
	for (std::list<Circle>::iterator i = RhythmCircles.begin(); i != RhythmCircles.end(); i++)
	{
		if (!i->isDrawable)
			continue;
		// draw the circle
		FCanvasTileItem TileItem(i->position, i->Texture->Resource, FLinearColor::White);
		TileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(TileItem);
	}
}

void ACrosshairCircle::DrawCrosshair()
{
	// draw the crosshair
	FCanvasTileItem TileItem(CrosshairPosition, CrosshairTex->Resource, FLinearColor::White);
	TileItem.BlendMode = SE_BLEND_Translucent;
	Canvas->DrawItem(TileItem);
}

/////////////////////////////////////////// TICK UPDATES
void ACrosshairCircle::Tick(float dt)
{
	Super::Tick(dt);
	if (!RhythmCircles.empty())
	{
		UpdateCirclePosition(dt);
		MakeCanPressedCircle();
		DeleteCircle();
	}
}

void ACrosshairCircle::UpdateCirclePosition(float dt)
{
	for (std::list<Circle>::iterator i = RhythmCircles.begin(); i != RhythmCircles.end(); i++)
	{
		if (!i->isDrawable && i->DeflectAngle >= Def_ANGLE)
			i->isDrawable = true;
		float dAlpha = i->Velocity * dt;
		i->position.X = ViewportCenter.X + HintRadius * cos(i->DeflectAngle + dAlpha) - CircleRadius;
		i->position.Y = ViewportCenter.Y + HintRadius * sin(i->DeflectAngle + dAlpha) - CircleRadius;
		i->DeflectAngle += dAlpha;
	}
}

void ACrosshairCircle::AddCircle(/*float DefAngle = */)
{
	Circle circle;
	circle.isPressed = false;
	circle.DeflectAngle = Def_ANGLE;
	circle.Velocity = StandartCircleVelocity;
	circle.Texture = StandartCircleTex;
	circle.isDrawable = false;
	RhythmCircles.push_back(circle);
}

void ACrosshairCircle::DeleteCircle()
{
	if (RhythmCircles.begin()->DeflectAngle - Def_ANGLE > 2 * PI * 1.03)
	{
		if (!RhythmCircles.begin()->isPressed || RhythmCircles.begin()->Texture == LostCircleTex)
		{
			//DecreaseCombo();
		}
		RhythmCircles.pop_front();
		if (RhythmCircles.empty())
		{
			StartRhythm();
		}
	}
}

void ACrosshairCircle::MakeCircleLost()
{
	RhythmCircles.begin()->Texture = LostCircleTex;
	RhythmCircles.begin()->isPressed = true;
	RhythmCircles.begin()->Velocity = LostCircleVelocity;
}

void ACrosshairCircle::MakeCanPressedCircle()
{
	if (RhythmCircles.begin()->isPressed)
		return;
	if (TargetAndCircleCollides())
	{
		RhythmCircles.begin()->Texture = CanPressedCircleTex;
	}
}

bool ACrosshairCircle::TargetAndCircleCollides()
{
	FVector2D CirclePosition = RhythmCircles.begin()->position;
	if (RhythmCircles.begin()->DeflectAngle - Def_ANGLE > PI * 1.9)
		return true;
	return false;
}

/////////////////////////////////// RHYTHM
bool ACrosshairCircle::InRhythmPressed()
{
	if (!RhythmCircles.empty())
	{
		if (RhythmCircles.begin()->Texture == CanPressedCircleTex && !RhythmCircles.begin()->isPressed)
		{
			RhythmCircles.begin()->isPressed = true;
			//IncreaseCombo();
			return true;
		}
		else
		{
			if(RhythmCircles.begin()->isDrawable)
				MakeCircleLost();
			//DecreaseCombo();
		}
	}
	return false;
}

//void ACrosshairCircle::DecreaseCombo()
//{
//	if (Combo != 0)
//		Combo--;
//}
//
//void ACrosshairCircle::IncreaseCombo()
//{
//	Combo++;
//}

// WITH TARRAY
//void ACrosshairCircle::StartRhythm()
//{
//	auto Data = USRhythm::GetData();
//	for (size_t i = 0; i < Data.Num(); i++)
//	{
//		float Angle = StandartCircleVelocity * Data[i].time;
//		float ToAdd = Def_ANGLE + (2 * PI - Angle);
//		AddCircle(ToAdd);
//	}
//}


void ACrosshairCircle::StartAdding()
{
	AddCircle();
	GetWorld()->GetTimerManager().SetTimer(TimeHandle, this, &ACrosshairCircle::AddCircle, DelayTime, true);
}

// WITH VECTOR
void ACrosshairCircle::StartRhythm()
{
	/*auto Data = ULRhythm::GetData();
	DelayTime = Data.BPS;
	GetWorld()->GetTimerManager().SetTimer(TimeHandle, this, &ACrosshairCircle::StartAdding, Data.StartDelay, false);*/

	/*for (size_t i = 0; i < Data.Num(); i++)
	{
		float Angle = StandartCircleVelocity * Data[i].time;
		float ToAdd = Def_ANGLE + (2 * PI - Angle);
		AddCircle(ToAdd);
	}*/
	//ULRhythm::ClearData();
}

void ACrosshairCircle::EndRhythm()
{
	RhythmCircles.clear();
	GetWorld()->GetTimerManager().ClearTimer(TimeHandle);
}

int ACrosshairCircle::CalculatePerfectionStateIndex()
{
	return 0;
}



