// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HUDS/RhythmCrosshair.h"
#include "Rhythm/LRhythm.h"
#include "CanvasItem.h"
#include <list>
#include "CrosshairLinear.generated.h"

struct Note
{
	static class UTexture* Texture;
	float CurrentWidth;
	float CurrentHeight;
	float Scale;
	FVector2D Position;
	bool IsReachedTarget;
};

UCLASS()
class RHYTHMSHOOTER_API ACrosshairLinear : public ARhythmCrosshair
{
	GENERATED_BODY()
public:
	ACrosshairLinear();
	/** Primary draw call for the HUD */
	virtual void BeginPlay() override;
	virtual void DrawHUD() override;
	virtual void Tick(float) override;
	/** StartRhythm*/
	virtual void StartRhythm() override;
	virtual void EndRhythm() override;
	/** Check for rhythm fire*/
	virtual bool InRhythmPressed() override;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Textures")
		class UTexture* NoteTex;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Textures")
		class UTexture* TargetTex;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Velocity")
		float Velocity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OffsetFromBottom")
		float Offset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "StartScale")
		float StartScale;
	
	virtual void AddNote() override;
	virtual void DeleteNote() override;

private:
	float ScaleVelocity;
	float ScaleVelocityAfterDestination;
	/** Note*/
	std::list<Note> RhythmCircles;
	FVector2D StartPosition;
	float NoteWidth;
	float NoteHeight;
	float NoteWidthHalf;
	float NoteHeightHalf;
	void UpdateCircles(float dt);
	void DrawNotes();
	/** Target*/
	float TargetWidth;
	float TargetHeight;
	float TargetHalfHeight;
	float TargetHalfWidth;
	float Dispersion; // distance of each perfection zone
	FVector2D TargetPosition;
	void DrawTarget();
	/** Viewport*/
	FVector2D ViewportCenter;
	float DistanceFromStartToTarget;
	/** Timing*/
	virtual int CalculatePerfectionStateIndex() override;
};
