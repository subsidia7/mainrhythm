// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/UMG.h"
#include "MushkaWidget.generated.h"

/**
 * 
 */
UCLASS()
class RHYTHMSHOOTER_API UMushkaWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UMushkaWidget(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	void Show();
	void Hide();	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		UImage* mushka;
};
