// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RhythmCrosshair.h"
#include "PerfectionWidget.generated.h"

/**
 * 
 */
UCLASS()
class RHYTHMSHOOTER_API UPerfectionWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UPerfectionWidget(const FObjectInitializer& ObjectInitializer);
	virtual void NativeConstruct() override;
	UFUNCTION(BlueprintImplementableEvent)
	void SetTextAndColor();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FPerfection perf;
};
