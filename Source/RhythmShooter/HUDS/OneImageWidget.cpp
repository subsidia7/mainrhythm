// Fill out your copyright notice in the Description page of Project Settings.


#include "OneImageWidget.h"

UOneImageWidget::UOneImageWidget(const FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer)
{
}

void UOneImageWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UOneImageWidget::Show()
{
	main_image->SetVisibility(ESlateVisibility::Visible);
	this->AddToViewport();
}

void UOneImageWidget::Hide()
{
	this->RemoveFromParent();
}

void UOneImageWidget::SetOpacity(double newOpacity)
{
	main_image->SetOpacity(newOpacity);
}