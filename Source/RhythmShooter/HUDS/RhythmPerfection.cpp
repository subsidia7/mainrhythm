// Fill out your copyright notice in the Description page of Project Settings.


#include "RhythmPerfection.h"
#include "RhythmShooterCharacter.h"
#include "Components/WidgetComponent.h"
#include "HUDS/PerfectionWidget.h"
#include "Kismet/KismetMathLibrary.h"
#include "HUDS/RhythmCrosshair.h"

// Sets default values
ARhythmPerfection::ARhythmPerfection()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PerfectionWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("PerfectionWidget"));
	PerfectionWidget->SetupAttachment(GetRootComponent());
	PerfectionWidget->SetWidgetClass(WidgetClass);
}

// Called when the game starts or when spawned
void ARhythmPerfection::BeginPlay()
{
	Super::BeginPlay();
	PerfectionWidget->SetWorldLocation(GetActorLocation());
	GetWorld()->GetTimerManager().SetTimer(Timer, this, &ARhythmPerfection::Destroying, TimeToDestroy, false);
}

void ARhythmPerfection::Destroying()
{
	Destroy();
}

void ARhythmPerfection::SetPlayer(ARhythmShooterCharacter* _Player)
{
	Player = _Player;
}

void ARhythmPerfection::SetTextAndColor(FPerfection& perfection)
{
	UUserWidget* widg = PerfectionWidget->GetUserWidgetObject();
	UPerfectionWidget* widgper = Cast<UPerfectionWidget>(widg);
	if(widgper)
	{
		widgper->perf = perfection;
		widgper->SetTextAndColor();
	}
}

// Called every frame
void ARhythmPerfection::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorLocation(GetActorLocation() + FVector(0, 0, Velocity * DeltaTime));
	FVector LocationSelf = GetActorLocation();
	FVector LocationP = Player->GetActorLocation();
	PerfectionWidget->SetWorldRotation(UKismetMathLibrary::FindLookAtRotation(LocationSelf, LocationP));

}

