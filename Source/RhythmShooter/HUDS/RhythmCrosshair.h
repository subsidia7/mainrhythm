// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "RhythmCrosshair.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FPerfection
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Perfection;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor Color;
};

UCLASS()
class RHYTHMSHOOTER_API ARhythmCrosshair : public AHUD
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
		virtual bool InRhythmPressed() { return false; }
	UFUNCTION(BlueprintCallable)
		virtual void StartRhythm() {}
	UFUNCTION(BlueprintCallable)
		virtual void EndRhythm() {}
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FPerfection> PerfectionStates;
	UFUNCTION(BlueprintCallable)
		FPerfection GetPerfection()
		{
			return PerfectionStates[PerfectionIndex];
		}
	virtual void AddNote() {}
	virtual void DeleteNote() {}
	float TimeToReachTarget;

protected:
	int PerfectionIndex;
	virtual int CalculatePerfectionStateIndex() { return 0; }
};
