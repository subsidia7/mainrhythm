// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetSystemLibrary.h"
#include "BasicEnemy.h"
#include "CharacterProjHitInfo.h"
#include "RhythmShooterProjectile.generated.h"

UCLASS(config=Game)
class ARhythmShooterProjectile : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	class USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;

public:
	ARhythmShooterProjectile();

	TSubclassOf<class ARhythmShooterProjectile> ProjectileClass;

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	//Damage, take it from gun
	float damage;
	int throughDamage;
	FVector CalculateExitLocation(AActor* OtherActor, const FHitResult Hit, bool* isFound);

	UFUNCTION(BlueprintCallable)
	void SetFireParameters(float newdamage, float speed, int newThroughDamage);
	void SetActualAndMaxSpeed(float actualSpeed, float maxSpeed);

	CharacterProjHitInfo* hitInfo;
	void SetHitInfo();
	ACharacter* owner;
};

